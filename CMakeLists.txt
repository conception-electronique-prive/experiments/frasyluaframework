cmake_minimum_required(VERSION 3.0.0)
project(FrasyLua VERSION 0.1.0)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(CMAKE_CXX_STANDARD 20)

include(CTest)
enable_testing()

include_directories(
        ${CMAKE_SOURCE_DIR}/vendor/lua/src
        ${CMAKE_SOURCE_DIR}/vendor/sol/include
)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/output)

FILE(GLOB LUA ${CMAKE_SOURCE_DIR}/vendor/lua/src/*.c)
LIST(FILTER LUA EXCLUDE REGEX ${CMAKE_SOURCE_DIR}/vendor/lua/src/luac.c)
LIST(FILTER LUA EXCLUDE REGEX ${CMAKE_SOURCE_DIR}/vendor/lua/src/lua.c)

add_executable(FrasyLua main.cpp team.cpp utils.cpp ${LUA})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
