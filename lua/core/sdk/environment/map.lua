--- @file    map.lua
--- @author  Paul Thomas
--- @date    3/20/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
local internal = {
    map   = {},
    scope = nil,
    count = {
        expected = { tp = 0, ib = 0, uut = 0, },
        recount  = { tp = 0, ib = 0, uut = 0, },
    },
    keys  = { tp = {}, ib = {}, uut = {} }
}

local mapper   = { TestPoint = {}, UUT = {}, IB = {} }

function mapper.validate()
    for _ in pairs(internal.keys.tp) do internal.count.recount.tp = internal.count.recount.tp + 1 end
    for _ in pairs(internal.keys.uut) do internal.count.recount.uut = internal.count.recount.uut + 1 end
    for _ in pairs(internal.keys.ib) do internal.count.recount.ib = internal.count.recount.ib + 1 end

    if internal.count.expected.tp ~= 0 and internal.count.expected.tp ~= internal.count.recount.tp then
        error(MapperError("Unexpected TP count. E: " .. internal.count.expected.tp .. ", C: " .. internal.count.recount.tp))
    end
    if internal.count.expected.uut ~= 0 and internal.count.expected.uut ~= internal.count.recount.uut then
        error(MapperError("Unexpected UUT count. E: " .. internal.count.expected.uut .. ", C: " .. internal.count.recount.uut))
    end
    if internal.count.expected.ib ~= 0 and internal.count.expected.ib ~= internal.count.recount.ib then
        error(MapperError("Unexpected IB count. E: " .. internal.count.expected.ib .. ", C: " .. internal.count.recount.ib))
    end

    for tp, uuts in pairs(internal.map) do
        local count = 0
        for uut, value in pairs(uuts) do
            local iuut = uut
            if internal.count.recount.uut < iuut then
                error(MapperError("Out of range UUT on " .. tp .. " : " .. iuut))
            end
            if internal.count.recount.ib < value.ib then
                error(MapperError("Out of range IB on " .. tp .. " on " .. uut .. ". Got " .. value.ib))
            end
            count = count + 1
        end
        if count ~= internal.count.recount.uut then
            error(MapperError("Invalid UUT count for " .. tp .. ". E: " .. internal.count.recount.uut .. ", C: " .. count))
        end
    end

    Context.Map = {
        count = {
            tp  = internal.count.recount.tp,
            uut = internal.count.recount.uut,
            ib  = internal.count.recount.ib,
        },
        ib    = {},
    }
    for tp, uuts in pairs(internal.map) do
        for uut, value in pairs(uuts) do
            if Context.Map.ib[uut] == nil then
                Context.Map.ib[uut] = value.ib
            elseif Context.Map.ib[uut] ~= value.ib then
                error(MapperError("Current version of Frasy does not support more than 1 IB per UUT"))
            end
        end
    end
    return internal.map
end

function mapper.TestPoint.New(name)
    internal.scope         = name
    internal.keys.tp[name] = 0
end

function mapper.TestPoint.To(uut, ib, tp)
    if internal.scope == nil then
        error(MapperError("No scope provided"))
    end
    if uut < 1 or (internal.count.expected.uut ~= 0 and internal.count.expected.uut < uut) then
        error(MapperError(string.format("Out of range UUT %d", uut)))
    end
    if ib < 1 or (internal.count.expected.ib ~= 0 and internal.count.expected.ib < ib) then
        error(MapperError(string.format("Out of range IB: %d", ib)))
    end
    if internal.map[internal.scope] == nil then
        internal.map[internal.scope] = {}
    end
    internal.keys.uut[uut]                     = 0
    internal.keys.ib[ib]                       = 0
    internal.map[internal.scope][uut] = { ib = ib, tp = tp }
    return { To = mapper.TestPoint.To }
end

function mapper.TestPoint.Count(count)
    if count ~= nil then internal.count.expected.tp = count end
    return internal.count.expected.tp
end

function mapper.UUT.Count(count)
    if count ~= nil then internal.count.expected.uut = count end
    return internal.count.expected.uut
end

function mapper.IB.Count(count)
    if count ~= nil then internal.count.expected.ib = count end
    return internal.count.expected.ib
end

return mapper;