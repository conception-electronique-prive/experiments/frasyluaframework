--- @file    testbench.lua
--- @author  Paul Thomas
--- @date    3/15/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.

Testbench = {
    __internal = {}
}

--- Request the testbench to execute a command
--- Allows testbench to know on which board to run a command
--- and to get the proper test point values to send
--- @param command string the name of the command to run
--- @param executor function
--- @param ... number test points required by the executor
--- @return any forward the result of the requested command
function Testbench:Exec(command, executor, ...)
    local tps = {}
    local ib = 0

    for index, tp in ipairs({ ... }) do
        tps[index] = tp[Context.uut].tp
        if ib ~= 0 and ib ~= tp[Context.uut].board then
            error("Requesting TP on different IB")
        end
        ib = tp[Context.uut].ib
    end

    if command == nil or executor == nil or #tps == 0 then
        error("Missing arguments")
    end

    if ib == 0 then
        error("No board selected")
    end

    Context.Exec.ib = ib

    if Testbench.__internal[command] == nil then
        error("Unknown command")
    end
    if Testbench.__internal[command].tp ~= #tps then
        error("Invalid number of TP provided")
    end
    while true do
        local status, r = pcall(Testbench.__internal[command].func, table.unpack(tps))
        if status then
            return r
        end
        -- TODO Check if recoverable
    end

end

--- Add a new command to the testbench
---
function Testbench:AddCommand(name, func, tp)
    if name == nil or type(name) ~= "string" then
        error("Missing or Invalid name argument")
    end
    if func == nil or type(func) ~= "function" then
        error("Missing or Invalid name func")
    end
    if tp == nil or type(tp) ~= "number" then
        error("Missing or Invalid name tp")
    end
    if Testbench.__internal["name"] ~= nil then
        error("Already exist")
    end
    Testbench.__internal["name"] = { func = func, tp = tp }
end