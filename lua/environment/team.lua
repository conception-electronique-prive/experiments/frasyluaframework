--- @file    team.lua
--- @author  Paul Thomas
--- @date    3/17/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.

Environment(function()
    Worker.Limit(IB).To(1, Team)
    for row = 1, 4 do
        local uutOffset = (row - 1) * 4
        Team.Join(uutOffset + 1, uutOffset + 2, uutOffset + 3, uutOffset + 4)
        TestPoint("TP1")
                .To(uutOffset + 01, 1, uutOffset + 1)
                .To(uutOffset + 02, 1, uutOffset + 2)
                .To(uutOffset + 03, 1, uutOffset + 3)
                .To(uutOffset + 04, 1, uutOffset + 4)
    end
end)