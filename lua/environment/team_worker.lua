--- @file    team_worker.lua
--- @author  Paul Thomas
--- @date    3/20/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.

Environment(function()
    TestPoint().Count(1)
    UUT.Count(55)
    IB.Count(6)

    Worker.Limit(IB).To(1, Team)

    for ib = 1, IB.Count() - 1 do
        local uutOffset = (ib - 1) * 10
        Team.Join(01 + uutOffset, 02 + uutOffset)
        Team.Join(03 + uutOffset, 04 + uutOffset)
        Team.Join(05 + uutOffset, 06 + uutOffset)
        Team.Join(07 + uutOffset, 08 + uutOffset)
        Team.Join(09 + uutOffset, 10 + uutOffset)

        TestPoint("TP1")
                .To(01 + uutOffset, ib, 01)
                .To(02 + uutOffset, ib, 02)
                .To(03 + uutOffset, ib, 03)
                .To(04 + uutOffset, ib, 04)
                .To(05 + uutOffset, ib, 05)
                .To(06 + uutOffset, ib, 06)
                .To(07 + uutOffset, ib, 07)
                .To(08 + uutOffset, ib, 08)
                .To(09 + uutOffset, ib, 09)
                .To(10 + uutOffset, ib, 10)
    end

    Team.Join(51)
    Team.Join(52)
    Team.Join(53)
    Team.Join(54)
    Team.Join(55)

    TestPoint("TP1")
            .To(51, IB.Count(), 1)
            .To(52, IB.Count(), 2)
            .To(53, IB.Count(), 3)
            .To(54, IB.Count(), 4)
            .To(55, IB.Count(), 5)
end)
