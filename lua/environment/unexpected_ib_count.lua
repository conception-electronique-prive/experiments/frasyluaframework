--- @file    valid.lua
--- @author  Paul Thomas
--- @date    3/17/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.

Environment(function()
    TestPoint().Count(3)
    UUT.Count(4)
    IB.Count(3)

    for ib = 1, 2 do
        local uutOffset = (ib - 1) *2
        TestPoint("TP1")
                .To(1 + uutOffset, ib, 1)
                .To(2 + uutOffset, ib, 4)

        TestPoint("TP2")
                .To(1 + uutOffset, ib, 2)
                .To(2 + uutOffset, ib, 5)

        TestPoint("TP3")
                .To(1 + uutOffset, ib, 3)
                .To(2 + uutOffset, ib, 6)
    end
end)