--- @file    script.lua
--- @author  Paul Thomas
--- @date    3/17/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.

Sequence("S1", function()
    for i = 1, 50 do
        Test("T" .. i, function()
            if i == 50 then Requires(Test("T" .. (i - 1)):ToPass())
            elseif i == 48 then Requires(Test("T" .. (i - 1)):ToBeBefore())
            elseif i > 1 then Requires(Test("T" .. (i - 1)):ToBeComplete()) end

            local r
            if (Team.IsLeader()) then
                if i == 49 and Context.stage == Stage.Execution then error(UnmetExpectation("Simulated failure")) end
                r = { 1, 2, 3, 4 }
                Team.Tell(r)
            else
                if i == 46 and Context.stage == Stage.Execution then error(UnmetExpectation("Simulated failure")) end
                r = Team.Get()
            end
            Expect(Utils.traverse(r, Team.Position())):ToBeEqual(Team.Position())
        end)
    end
end)