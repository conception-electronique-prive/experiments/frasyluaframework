--- @file    script.lua
--- @author  Paul Thomas
--- @date    3/21/2023
--- @brief
---
--- @copyright
--- This program is free software: you can redistribute it and/or modify it under the
--- terms of the GNU General Public License as published by the Free Software Foundation, either
--- version 3 of the License, or (at your option) any later version.
--- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
--- even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--- General Public License for more details.
--- You should have received a copy of the GNU General Public License along with this program. If
--- not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.

Sequence("S1", function()
    Log.i("S1", tostring(Context.uut))
    Test("T1", function() Log.i("S1-T1", tostring(Context.uut)) end)
    Test("T2", function() Log.i("S1-T2", tostring(Context.uut)) end)
    Test("T3", function() Log.i("S1-T3", tostring(Context.uut)) end)
end)

Sequence("S2", function()
    Log.i("S2", tostring(Context.uut))
    Test("T1", function() Log.i("S2-T1", tostring(Context.uut)) end)
    Test("T2", function() Log.i("S2-T2", tostring(Context.uut)) end)
    Test("T3", function() Log.i("S2-T3", tostring(Context.uut)) end)
end)