#include <iostream>
#include <string_view>
#include <sol/sol.hpp>
#include <vector>
#include <map>
#include <thread>
#include <filesystem>
#include <mutex>
#include "team.h"

static constexpr std::string_view output = "logs";

void make_output_dir() {
    namespace fs = std::filesystem;
    fs::create_directory(output);
}

void register_log(sol::state &lua, std::size_t uut) {
    enum Level {
        C, E, W, I, D
    };

    static std::mutex logm = std::mutex();
    auto log = [](Level level, std::size_t uut, const std::string &tag, const std::string &message) {
        std::lock_guard guard{logm};
        switch (level) {
            case C:std::cout << "\033[97;101mC ";
                break;
            case E:std::cout << "\033[91mE ";
                break;
            case W:std::cout << "\033[93mW ";
                break;
            case I:std::cout << "\033[32mI ";
                break;
            case D:std::cout << "\033[37mD ";
                break;
        }
        std::cout << std::to_string(uut) << " [" << tag << "] " << message << "\033[0m\r\n";
    };

    lua["Log"] = lua.create_table();
    lua["Log"]["c"] = [log, uut](const std::string &tag, const std::string &message) { log(C, uut, tag, message); };
    lua["Log"]["e"] = [log, uut](const std::string &tag, const std::string &message) { log(E, uut, tag, message); };
    lua["Log"]["w"] = [log, uut](const std::string &tag, const std::string &message) { log(W, uut, tag, message); };
    lua["Log"]["i"] = [log, uut](const std::string &tag, const std::string &message) { log(I, uut, tag, message); };
    lua["Log"]["d"] = [log, uut](const std::string &tag, const std::string &message) { log(D, uut, tag, message); };
}

void initialize_state(sol::state &lua, std::size_t uut = 0, const std::string &state = "Generation") {
    lua.open_libraries(sol::lib::debug,
                       sol::lib::base,
                       sol::lib::table,
                       sol::lib::io,
                       sol::lib::package,
                       sol::lib::string,
                       sol::lib::math,
                       sol::lib::os);

    // Enums
    lua.script_file("lua/core/framework/stage.lua");

    // Variables
    lua.script_file("lua/core/framework/context.lua");
    lua.script_file("lua/core/sdk/map.lua");

    lua["Context"]["stage"] = lua["Stage"][state];
    lua["Context"]["uut"] = uut;
    lua["Context"]["version"] = "0.1.0";
    std::atomic_thread_fence(std::memory_order_release);

    // Utils
    lua.require_file("Utils", "lua/core/utils/module.lua");
    lua["Utils"]["dirlist"] = [](const std::string &dir) {
        using namespace std::filesystem;
        std::vector<std::string> files;
        for (auto const &dir_entry : directory_iterator{dir}) {
            auto file = dir_entry.path().string();
            files.push_back(file.substr(0, file.size() - 4));
        }
        return sol::as_table(files);
    };
    lua["Utils"]["sleep_for"] = [](int duration) { std::this_thread::sleep_for(std::chrono::milliseconds(duration)); };
    lua.require_file("Json", "lua/core/vendor/json.lua");
    register_log(lua, uut);
    lua.script_file("lua/core/framework/exception.lua");

    // Framework
    lua.script_file("lua/core/sdk/environment/team.lua");
    lua.script_file("lua/core/sdk/environment/environment.lua");
    lua.script_file("lua/core/sdk/testbench.lua");
    lua.script_file("lua/core/framework/orchestrator.lua");
    lua.script_file("lua/core/sdk/framework.lua");
}

bool loadEnvironment(sol::state &lua, const std::string &environment) {
    sol::protected_function run = lua.script_file("lua/core/helper/load_environment.lua");
    run.error_handler = lua.script_file("lua/core/framework/error_handler.lua");
    auto result = run(environment);
    if (!result.valid()) {
        sol::error err = result;
        lua["Log"]["e"]("Exception", err.what());
    }
    else {
        lua["Log"]["i"]("Environment", "Loaded successfully");
    }
    return result.valid();
}

bool loadTests(sol::state &lua, const std::string &tests) {
    sol::protected_function run = lua.script_file("lua/core/helper/load_tests.lua");
    run.error_handler = lua.script_file("lua/core/framework/error_handler.lua");
    auto result = run(tests);
    if (!result.valid()) {
        sol::error err = result;
        lua["Log"]["e"]("Exception", err.what());
    }
    else {
        lua["Log"]["i"]("Tests", "Loaded successfully");
    }
    return result.valid();
}

bool doStep(sol::state &lua, const std::string &step) {
    sol::protected_function run = lua.script_file(step);
    run.error_handler = lua.script_file("lua/core/framework/error_handler.lua");
    auto result = run(output);
    if (!result.valid()) {
        sol::error err = result;
        lua["Log"]["e"]("Exception", err.what());
    }
    else {
        lua["Log"]["i"]("Run", "Success");
    }
    return result.valid();
}

void TestMapping() {
    auto map = [](const std::string &map, const std::string &message) {
        sol::state lua;
        initialize_state(lua);
        std::cout << message << std::endl;
        loadEnvironment(lua, map);
    };

    map("lua/environment/valid", "Valid");
    map("lua/environment/unexpected_tp_count", "Invalid TP count");
    map("lua/environment/unexpected_uut_count", "Invalid UUT count");
    map("lua/environment/unexpected_ib_count", "Invalid IB count");
    map("lua/environment/gap_uut", "Invalid UUT count on TP");
    map("lua/environment/invalid_recount_tp", "Invalid recount TP");
    map("lua/environment/invalid_recount_uut", "Invalid recount UUT");
    map("lua/environment/invalid_recount_ib", "Invalid recount IB");
    map("lua/environment/out_of_range_uut_l", "Out of range UUT L");
    map("lua/environment/out_of_range_uut_h", "Out of range UUT H");
    map("lua/environment/out_of_range_ib_l", "Out of range IB L");
    map("lua/environment/out_of_range_ib_h", "Out of range IB H");
}

void TestOrder() {
    auto test = [](const std::string &test, const std::string &message) {
        sol::state lua;
        initialize_state(lua);
        std::cout << message << std::endl;
        doStep(lua, "lua/core/helper/generate.lua");
    };
    test("lua/tests/order/valid", "Valid");
}

void TestTeamWorker() {
    static constexpr auto environment = "lua/environment/team_worker";
    static constexpr auto tests = "lua/tests/team/worker";
    sol::state team;
    initialize_state(team);
    loadEnvironment(team, environment);

    sol::state generate;
    initialize_state(generate);
    loadEnvironment(generate, environment);
    loadTests(generate, tests);
    doStep(generate, "lua/core/helper/generate.lua");

    std::vector<std::thread> threads;
    auto stages = team["Context"]["Worker"]["stages"].get<std::vector<sol::object>>();
    for (sol::object &stage : stages) {
        auto devices = stage.as<std::vector<sol::object>>();
        for (sol::object &device : devices) {
            threads.emplace_back([&]() {
                sol::state lua;
                initialize_state(lua, device.as<int>(), "Validation");
                loadEnvironment(lua, environment);
                loadTests(lua, tests);
                doStep(lua, "lua/core/helper/validate.lua");
            });
        }
        for (auto &thread : threads) thread.join();
        threads.clear();
    }

    for (sol::object &stage : stages) {
        auto devices = stage.as<std::vector<sol::object>>();
        for (sol::object &device : devices) {
            threads.emplace_back([&]() {
                sol::state lua;
                initialize_state(lua, device.as<int>(), "Execution");
                loadEnvironment(lua, environment);
                loadTests(lua, tests);
                doStep(lua, "lua/core/helper/execute.lua");
            });
        }
        for (auto &thread : threads) thread.join();
        threads.clear();
    }
}

void TestTeam() {
    static constexpr auto environment = "lua/environment/team";
    static constexpr auto tests = "lua/tests/team/valid";
    sol::state team;
    initialize_state(team);
    loadEnvironment(team, environment);
    bool hasTeam = team["Team"]["HasTeam"]();

    sol::state generate;
    initialize_state(generate, 1);
    loadEnvironment(generate, environment);
    loadTests(generate, tests);
    doStep(generate, "lua/core/helper/generate.lua");

    auto stages = team["Context"]["Worker"]["stages"].get<std::vector<sol::object>>();
    for (sol::object &stage : stages) {
        std::vector<std::thread> threads;
        auto devices = stage.as<std::vector<sol::object>>();
        std::map<std::size_t, Team> teams;
        if (hasTeam) {
            for (sol::object &device : devices) {
                std::size_t uut = device.as<std::size_t>();
                std::size_t leader = team["Context"]["Team"]["players"][uut]["leader"];
                auto teamPlayers = team["Context"]["Team"]["teams"][leader].get<std::vector<std::size_t>>();
                if (leader == uut) teams[leader] = Team(teamPlayers.size());
            }
        }
        std::mutex mutex;
        for (sol::object &device : devices) {
            threads.emplace_back([&](std::size_t uut) {
                sol::state lua;
                initialize_state(lua, uut, "Validation");
                loadEnvironment(lua, environment);
                loadTests(lua, tests);
                if (hasTeam) {
                    std::lock_guard lock{mutex};
                    int leader = team["Context"]["Team"]["players"][uut]["leader"];
                    int position = team["Context"]["Team"]["players"][uut]["position"];
                    teams[leader].InitializeState(lua, uut, position, uut == leader);
                }
                doStep(lua, "lua/core/helper/validate.lua");
            }, device.as<std::size_t>());
        }
        for (auto &thread : threads) thread.join();
        threads.clear();

        for (sol::object &device : devices) {
            threads.emplace_back([&]() {
                sol::state lua;
                initialize_state(lua, device.as<int>(), "Execution");
                loadEnvironment(lua, environment);
                loadTests(lua, tests);
                if (hasTeam) {
                    std::lock_guard lock{mutex};
                    int uut = device.as<int>();
                    int leader = team["Context"]["Team"]["players"][uut]["leader"];
                    int position = team["Context"]["Team"]["players"][uut]["position"];
                    teams[leader].InitializeState(lua, uut, position, uut == leader);
                }
                doStep(lua, "lua/core/helper/execute.lua");
            });
        }
        for (auto &thread : threads) thread.join();
        break;
    }
}

int main() {
    make_output_dir();

//    TestMapping();
//    TestOrder();
//    TestTeamWorker();
    TestTeam();

//    if (!Generate()) return -1;
//    if (!Validate()) return -1;
//    Execute();

    return 0;
}