import os
import pathlib
import shutil

root_dir = os.path.abspath(pathlib.Path(__file__).parent.parent.resolve())
dst = os.path.join(root_dir, "output", "lua")
src = os.path.join(root_dir, "lua")

shutil.rmtree(dst, ignore_errors=True)
shutil.copytree(src=src, dst=dst)
